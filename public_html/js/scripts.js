function sendOrder() {
    var order = $('#order input').serializeArray();

    if (order[0].value == '') {
        alert('Введите номер телефона');
        return false;
    }
    if (order[1].value == '') {
        alert('Введите email');
        return false;
    }

    $.post('/send.php', order, function (r) {
        if (r == 'success') {
            $('#order').html('<div id="success">Ваша заявка отправлена</div>');
            setTimeout(hideOverlay, 2000);
        }
        if (r == 'error') {
            alert('Ошибка при отправлении email');
        }
    });

}

function hideOverlay() {
    $('#overlay').hide();
}

function showOverlay() {
    $('#overlay').css('display', 'flex');
}

function showForm() {
    showOverlay();
}



$(document).ready(function () {

    $("#phone").mask("+7(999)999-9999");
    $('#overlay').on('click', function (e) {
        if (e.target.id == 'overlay') {
            hideOverlay();
        }
    });
});


